<?php

/**
 * Created by PhpStorm.
 * User: samparsky
 * Date: 4/17/16
 * Time: 11:14 AM
 */
namespace App\Helpers\Contracts;

Interface MakeRequestContract
{
    public function get($uri , $data);
    public function post($uri , $data);
    public function delete($uri , $data);
    public function patch($uri , $data);
    public function put($uri , $data);
    public function test();
}