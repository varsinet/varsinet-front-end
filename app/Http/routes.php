<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'teach'], function () {

	Route::match(['get','post'],'login', '\App\Http\Controllers\Teach\LoginController@login')
		->name('teachLogin');

	Route::match(['get', 'post'],'register','\App\Http\Controllers\Teach\RegisterController@register')
		->name('teachRegister');

	Route::get('register/success', 'RegisterController@success')
		->name('teachRegisterSuccess');

	Route::get('dashboard','\App\Http\Controllers\Teach\DashboardController@dashboard')->name('teachDashboard');

	Route::get('/profile','\App\Http\Controllers\Teach\ProfileController@profile')->name('teachProfile');

	Route::group(['prefix' => 'course'], function () {
		Route::get('create', '\App\Http\Controllers\Teach\CourseController@create')->name('teachCreateCourse');
		Route::get('{link}','\App\Http\Controllers\Teach\CourseController@getCourse')->name('GetCourse');
	});

	Route::get('test','LoginController@test');
});
