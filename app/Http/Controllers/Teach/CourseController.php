<?php
/**
 * Created by PhpStorm.
 * User: samparsky
 * Date: 4/17/16
 * Time: 2:01 PM
 */

namespace App\Http\Controllers\Teach;


use App\Helpers\Contracts\MakeRequestContract;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function create(Request $request){
        $data = $request->session()->get('user');
        if($request->getMethod() == 'POST'){
            $data = $request->all();
            var_dump($data);
        }
        return view('teach/course/index',$data);
    }

    public function getCourse(Request $request , $link , MakeRequestContract $requestContract){
        $data = $request->session()->get('user');
        if(!empty($link)){
            $result = $requestContract->get('/course/'.$link,[]);
            if((int)$result->statusCode == 200){
                $data['course'] = $result->response['data'];
                //var_dump($data['course']);
            } else {
                abort(404);
            }
        } else {
            abort(404);
        }

        return view('teach/course/view_course',$data);
    }


}