<?php
namespace App\Http\Controllers\Teach;
use App\Helpers\Contracts\MakeRequestContract;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class RegisterController extends Controller {

	public function register(Request $request,MakeRequestContract $make){
		if($request->method() == 'POST'){
			$validator = Validator::make($request->all(),[
				'firstname' => 'required',
				'lastname'  => 'required',
				'email'     => 'required',
				'password'  => 'required',
				'username'  => 'required'
			]);
			if($validator->fails()){
				return redirect('/register')
					->withInput()
					->withErrors($validator);
			} else {
				$data = [
					'firstname' => $request->input('firstname'),
					'lastname'  => $request->input('lastname'),
					'email'     => $request->input('email'),
					'password'  => $request->input('password'),
					'username'  => $request->input('username')
				];
				$response = $make->post('/student',$data);
				if($response->statusCode == 200){
					return redirect('/register/success')
						->with('email', [$data['email']]);
				} else if($response->statusCode == 400){
					var_dump($response);
					exit();
				}
			}
		}
		return view('teach/signup/index');
	}

	public function success(){
		$data = [];
		$data['email'] = session('email')[0];
		echo session('email')[0];
		return view('signup/success',compact($data));
	}
}