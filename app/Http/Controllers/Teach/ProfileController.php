<?php
/**
 * Created by PhpStorm.
 * User: samparsky
 * Date: 4/27/16
 * Time: 12:22 AM
 */

namespace App\Http\Controllers\Teach;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function profile(Request $request){
        $data = $request->session()->get('user');
        //var_dump($data);
        return view('teach/profile/index',$data);
    }

}