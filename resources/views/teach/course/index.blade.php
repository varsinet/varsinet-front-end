@extends('teach/master')
@section('content')
    <section id="main-content">
        <section class="wrapper" style="margin-top: 40px;">
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-3">
                        <a href="#/" class="active">
                        <h3 class="text-center">1</h3>
                        <p class="text-center">Course Basic information</p>
                        </a>
                    </div>
                    <div class="col-lg-3">
                        <a href="#/course" style="color:#89817e;">
                        <h3 class="text-center">2</h3>
                        <p class="text-center">Create Chapters</p>
                        </a>
                    </div>
                    <div class="col-lg-3">
                        <a href="#/topic" style="color:#89817e;">
                        <h3 class="text-center">3</h3>
                        <p class="text-center">Create Topics</p>
                        </a>
                    </div>
                    <div class="col-lg-3">
                        <a href="#/quiz" style="color:#89817e;">
                        <h3 class="text-center">4</h3>
                        <p class="text-center">Create Quizzes</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12" ng-view>
                </div>
            </div>
        </section>
    </section>

    </section>
@endsection