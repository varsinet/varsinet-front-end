@include('teach/include/header')
<body class="login-img2-body">
    <div class="container">
      <form method="post" class="login-form" style="margin-top:3%" action="{{ route('teachRegister') }}">
        <div class="login-wrap">
		   <h3 align="center">Varsinet Register</h3>
        {{ csrf_field() }}
            <p class="login-img"><i class="icon_lock_alt"></i></p>
            @include('teach.common.errors')
            <div class="input-group">
              <span class="input-group-addon"><i class="icon_profile"></i></span>
              <input type="text" name="firstname" class="form-control" placeholder="Firstname" autofocus>
            </div>
            <div class="input-group">
              <span class="input-group-addon"><i class="icon_profile"></i></span>
              <input type="text" name="lastname" class="form-control" placeholder="Lastname">
            </div>
            <div class="input-group">
              <span class="input-group-addon"><i class="icon_profile"></i></span>
              <input type="text" name="username" class="form-control" placeholder="Username">
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                <input type="email" name="email" class="form-control" placeholder="Email">
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                <input type="password" name="password" class="form-control" placeholder="Password">
            </div>
            <label class="checkbox">
                <input type="checkbox" value="remember-me"> Remember me
                <span class="pull-right"> <a href="#"> Forgot Password?</a></span>
            </label>
            <button class="btn btn-primary btn-lg btn-block" type="submit">Signup</button>
            <div style="margin-left: auto;margin-right:auto;margin-top:5%;">
            <button class="btn btn-info btn-lg center" type="submit">Login</button>
            </div>
        </div>
      </form>

    </div>
    @include('teach/include/footer')
  </body>