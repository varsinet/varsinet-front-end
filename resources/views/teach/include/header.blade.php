<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Karmanta - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Karmanta, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Varsinet - Lecturer Dashboard</title>
    
    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
    
    <link href="{{ URL::asset('css/bootstrap-theme.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/elegant-icons-style.css')}}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />

    <link rel="stylesheet" href="{{ URL::asset('css/owl.carousel.css') }}" type="text/css">

    
    <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/style-responsive.css') }}" rel="stylesheet" />


    <script src="{{ URL::asset('components/angular/angular.min.js') }}"></script>
    <script src="{{ URL::asset('components/angular-route/angular-route.min.js') }}"></script>
    <script src="{{ URL::asset('js/ng-file-upload.js') }}"></script>

    <script src="{{ URL::asset('js/app/main.js') }}"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
      <script src="{{ URL::asset('js/html5shiv.js') }}"></script>
      <script src="{{ URL::asset('js/respond.min.js') }}"></script>
      <script src="{{ URL::asset('js/lte-ie7.js') }}"></script>
    <![endif]-->
  </head>
